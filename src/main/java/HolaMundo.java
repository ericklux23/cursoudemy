
import java.util.Scanner;

public class HolaMundo {

    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);

        int x = 5;
        int y = 10;
        int z = ++x + y--;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.println("z = " + z);
        
        System.out.println("\n");
        int resultado = 4 + ((5 * 6) / 3);
        System.out.println("resultado = " + resultado);
        
        /*Scanner scan = new Scanner(System.in);

        byte num = 127;
        System.out.println("num = " + num);
        System.out.println("bites:" + Byte.SIZE);
        System.out.println("bites:" + Byte.BYTES);
        System.out.println("bites min:" + Byte.MIN_VALUE);
        System.out.println(" bites max:" + Byte.MAX_VALUE);
        System.out.println("\n");

        short num2 = 1000;
        System.out.println("num2 = " + num2);
        System.out.println("bites short:" + Short.SIZE);
        System.out.println("bites short:" + Short.BYTES);
        System.out.println("short min: " + Short.MIN_VALUE);
        System.out.println("short max:" + Short.MAX_VALUE);
        System.out.println("\n");
        int nume3 = 35000;
        System.out.println("nume3 = " + nume3);
        System.out.println("int short" + Integer.SIZE);
        System.out.println("int short" + Integer.BYTES);
        System.out.println("int min " + Integer.MIN_VALUE);
        System.out.println("int max" + Integer.MAX_VALUE);
        System.out.println("\n");
        long num4 = 9223372036854775807L;
        System.out.println("num4 = " + num4);
        System.out.println("long short" + Long.SIZE);
        System.out.println("long short" + Long.BYTES);
        System.out.println("long min " + Long.MIN_VALUE);
        System.out.println("long max" + Long.MAX_VALUE);
        Scanner scan = new Scanner(System.in);

        int decimal = 10;
        System.out.println("decimal = " + decimal);

        int numeroOctal = 012;
        System.out.println("numeroOctal = " + numeroOctal);

        int numeroHexadecimal = 0xA;
        System.out.println("numeroHexadecimal = " + numeroHexadecimal);

        int numeroBinario = 0b1010;
        System.out.println("numeroBinario = " + numeroBinario);
        float floatVar = 1000.10F;
        System.out.println("floatVar = " + floatVar);

        System.out.println("bites float" + Float.SIZE);
        System.out.println("bytes float" + Float.BYTES);
        System.out.println("float min" + Float.MIN_VALUE);
        System.out.println("float max" + Float.MAX_VALUE);

        double doubleVAr = 100D;
        System.out.println("doubleVAr = " + doubleVAr);

        System.out.println("bites double" + Double.SIZE);
        System.out.println("bytes double" + Double.BYTES);
        System.out.println("double min" + Double.MIN_VALUE);
        System.out.println("double max" + Double.MAX_VALUE);
        System.out.println("bites float" + Character.SIZE);
        System.out.println("bytes float" + Character.BYTES);
        System.out.println("float min" + Character.MIN_VALUE);
        System.out.println("float max" + Character.MAX_VALUE);

        char hola = '\u0021';
        System.out.println("hola = " + hola);

        char varChar = 33;
        System.out.println("varChar = " + varChar);

        char otro = '!';
        System.out.println("otro = " + otro);
        Scanner scan = new Scanner(System.in);

        System.out.println("true boolean" + Boolean.TRUE);
        System.out.println("false boolean" + Boolean.FALSE);

        boolean hello = true;
        if (hello == true) {
            System.out.println("el valor es verdadero");
        } else {
            System.out.println("el valor es falso");
        }

        System.out.println("Ingrese el primer digito");
        int a = Integer.parseInt(scan.nextLine());
        System.out.println("Ingrese el segundo digito");
        int b = Integer.parseInt(scan.nextLine());

        if (a > b) {
            System.out.println("el numero mayor es " + a);
        } else {
            System.out.println("el numero mayor es " + b);
        }

        int edad = 15;
        if (edad >= 18) {
            System.out.println("Si es un Adulto");
        } else {
            System.out.println("no es un adulto");
        }
        boolean adulto = edad >= 18;
        System.out.println("adulto = " + adulto);
        int edad = Integer.parseInt("5");
        System.out.println("edad = " + edad);

        double valor = Double.parseDouble("3.1416");
        System.out.println("valor = " + valor);

        char c = "hola".charAt(1);
        System.out.println("c = " + c);

        edad = Integer.parseInt(scan.nextLine());
        System.out.println("edad = " + edad);

        char caracter = scan.nextLine().charAt(0);
        System.out.println("caracter = " + caracter);

        String edad1 = String.valueOf(25);
        System.out.println("edad1 = " + edad1);

        short s = 129;
        byte x = (byte) s;
        System.out.println("x = " + x);
        int a = 3, b = 2;

        int resultado = a + b;
        System.out.println("resultado = " + resultado);

        resultado = a - b;
        System.out.println("resultado = " + resultado);

        resultado = a * b;
        System.out.println("resultado = " + resultado);

        float nuevo = 3F / b;
        System.out.println("nuevo = " + nuevo);

        resultado = a % b;
        System.out.println("resultado = " + resultado);

        resultado = a % 2;
        System.out.println("resultado = " + resultado);

        if (resultado == 0) {
            System.out.println("numero par");
        } else {
            System.out.println("numero impar");
        }
        int a = 3, b = 2;

        int c = a;
        System.out.println("c = " + c);

        a += 1;
        System.out.println("a = " + a);

        b -= 1;
        System.out.println("b = " + b);

        a *= 5;
        System.out.println("a = " + a);

        b /= 1;
        System.out.println("b = " + b);

        a %= 2;
        System.out.println("a = " + a);
        int a = 3;
        int b = 2;

        boolean c = (a == b);
        System.out.println("c = " + c);

        c = (a != b);
        System.out.println("c = " + c);

        String cadena = "hola";
        String cadena2 = "adios";
        System.out.println(cadena.equals(cadena2));

        boolean d = a < b;
        System.out.println("d = " + d);

        boolean e = a >= b;
        System.out.println("e = " + e);

        if (a % 2 == 0) {
            System.out.println("Es un numero Par");
        } else {
            System.out.println("Es un numero Impar");
        }

        int edad = 19;
        int adulto = 18;

        if (edad >= adulto) {
            System.out.println("Es un Adulto");
        } else {
            System.out.println("Es menor de Edad");
        }

        int a = 12;
        int min = 0, max = 10;
        boolean resultado = a >= min && a <= max;
        System.out.println("resultado = " + resultado);

        boolean vaciones = false;
        boolean diaDescanso = true;

        if (vaciones || diaDescanso) {
            System.out.println("Si puede asistir");
        } else {
            System.out.println("no puede asistir");
        }
        boolean resultado = (3 > 2) ? true : false;
        System.out.println("resultado = " + resultado);

        int numero = 10;
        String par = (numero % 2 == 0) ? "Numero par" : "Numero impar";
        System.out.println("numero " + par);*/
 public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);

//        for (int i = 0; i < 3; i++) {
//            if (i % 2 == 0) {
//                System.out.println("contador:" + i);
//            }
//
//        }
        inicio:
        for (int i = 0; i < 3; i++) {
            if (i % 2 != 0) {
                continue inicio;
            }
            System.out.println("i"+i);
        }

	boolean condicion = false;

        if (condicion) {
            System.out.println(" Es verdadero ");
        } else {
            System.out.println("Es falso");
        }

        int numero = 2;
        String texto = "numoro desconocido";
        
        if (numero == 1) {
            texto = "numero uno";
        } else if (numero == 2) {
            texto = "numero dos";
        } else if (numero == 3) {
            texto = "numero tres";
        } else {
            texto = "numero desconocido";
        }
        System.out.println(texto);


System.out.println("Ingrese el numero del mes que desea");
        int mes = scan.nextInt();
        String estacion;
        
        if(mes == 1 || mes == 2 || mes == 12){
            estacion = "Invierno";
        }else if (mes == 3 || mes == 4 || mes == 5){
            estacion = "Primavera";
        }else if(mes == 6 || mes == 7 || mes == 8){
            estacion = "Verano";
        }else if(mes == 9 || mes == 10 || mes == 11){
            estacion = "Otoño";
        }else{
            estacion = "Mes Incorrectp";
        }
        System.out.println("estacion:" + " " + estacion + " " +"para el mes" + " " +mes);
   


        int numero = 1;
        String texto = "numero desconocido";

        switch (numero) {
            case 1:
                texto = "Uno";
                break;
            case 2:
                texto = "Dos";
                break;
            case 3:
                texto = "Tres";
                break;
            default:
                texto = "Numero Desconocido";
        }
        
        System.out.println("Numoro convertido a Texto" + " " +texto + " " +"para el numero proporcionado" + " " + numero);
    


System.out.println("Ingrese el numero del mes");
        int mes = scan.nextInt();
        String estacion;

        switch (mes) {
            case 1: case 2: case 12:
                estacion = "Invierno";
                break;
            case 3:  case 4: case 5:
                estacion = "Primavera";
                break;
            case 6: case 7: case 8:
                estacion = "Verano";
                break;
            case 9: case 10: case 11:
                estacion = "otoño";
                break;
            default: 
                estacion = "Valor incorrecto";
                    
        }
        System.out.println("Estacion de:"+" "+ estacion+" "+"para el mes"+" "+mes);
    




int contador = 0;

        /* while (contador < 6) {
            System.out.println("ciclo infinito while"+ contador);
            contador++;
        }*/
        do {
            System.out.println("ciclo infinito while" + contador);
            contador++;
        } while (contador < 3);{

        }


for (int i = 0; i < 10; i++) {
            
        }
        
        int a = 0;
        
        while (a < 4){
            System.out.println("contador" + a);
            a++;
        }
    



 for (int i = 0; i < 3; i++) {
            if (i % 2 == 0) {
                System.out.println("contador:" + i);
            }

        }
        
        for (int i = 0; i < 3; i++) {
            if (i % 2 != 0) {
                continue;
            }
            System.out.println("i"+i);
        }

  
    inicio:
        for (int i = 0; i < 3; i++) {
            if (i % 2 != 0) {
                continue inicio;
            }
            System.out.println("i"+i);
        }

    }
}
